package android.odoshare.apponim.com.odoshare.recycler_view;

import android.content.Context;
import android.content.Intent;
import android.odoshare.apponim.com.odoshare.DocumentsActivity;
import android.odoshare.apponim.com.odoshare.R;
import android.odoshare.apponim.com.odoshare.entities.LocationEntity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivan on 9/24/2016.
 * <p/>
 * This class is property of APPONIM
 * apponim.com
 */
public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.MainItemViewHolder> {

    private LayoutInflater inflater;

    private List<LocationEntity> data = new ArrayList<LocationEntity>();

    private Context context;

    private ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

    public LocationListAdapter(Context context, List<LocationEntity> data){
        inflater = LayoutInflater.from(context);

        this.data = data;
        this.context=context;
    }

    @Override
    public MainItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_location, parent, false);
        MainItemViewHolder holder = new MainItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MainItemViewHolder holder, int position) {
        LocationEntity current = data.get(position);

        holder.name.setText(current.name);
        holder.distance.setText((int)(Math.random()*100) + "m");
        holder.fileCount.setText(current.fileCount + " FILES");

        int color = colorGenerator.getColor(current.name);
        holder.icon.setImageDrawable(TextDrawable.builder().buildRound(""+current.name.charAt(0), color));

        holder.fileCount.setText(current.fileCount>0 ? current.fileCount+" FILES" : "NO FILES");
        holder.fileCount.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MainItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        TextView fileCount;
        TextView distance;
        ImageView icon;

        public MainItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.location_item_name);
            fileCount = (TextView) itemView.findViewById(R.id.location_item_count);
            distance = (TextView) itemView.findViewById(R.id.location_item_distance);
            icon = (ImageView) itemView.findViewById(R.id.location_item_icon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent myIntent = new Intent(context, DocumentsActivity.class);
            myIntent.putExtra("locationId", data.get(getAdapterPosition()).id);
            myIntent.putExtra("locationName", data.get(getAdapterPosition()).name);
            myIntent.putExtra("latitude", data.get(getAdapterPosition()).latitude);
            myIntent.putExtra("longitude", data.get(getAdapterPosition()).longitude);
            context.startActivity(myIntent);
        }
    }

}

