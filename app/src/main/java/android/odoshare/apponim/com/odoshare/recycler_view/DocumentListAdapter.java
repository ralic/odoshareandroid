package android.odoshare.apponim.com.odoshare.recycler_view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.odoshare.apponim.com.odoshare.DocumentsActivity;
import android.odoshare.apponim.com.odoshare.R;
import android.odoshare.apponim.com.odoshare.entities.DocumentEntity;
import android.odoshare.apponim.com.odoshare.file_requests.AsyncDownload;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivan on 9/24/2016.
 * <p/>
 * This class is property of APPONIM
 * apponim.com
 */


public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.MainItemViewHolder> {

    private LayoutInflater inflater;

    private List<DocumentEntity> data = new ArrayList<>();

    private DocumentsActivity documentsActivity;

    private ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

    public DocumentListAdapter(DocumentsActivity documentsActivity, List<DocumentEntity> data){
        inflater = LayoutInflater.from(documentsActivity);

        this.data = data;
        this.documentsActivity=documentsActivity;
    }

    @Override
    public MainItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_document, parent, false);
        MainItemViewHolder holder = new MainItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MainItemViewHolder holder, int position) {
        DocumentEntity current = data.get(position);

        String [] filename = current.name.split("\\.(?=[^\\.]+$)");

        holder.name.setText(filename[0]);

        String extension;

        if(filename.length<2)
            extension = "?";
        else
            extension = filename[1];

        int color = colorGenerator.getColor(current.name);
        holder.icon.setImageDrawable(TextDrawable.builder().buildRoundRect(extension.toUpperCase(), color,4));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MainItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        ImageView icon;

        public MainItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.document_item_name);
            icon = (ImageView) itemView.findViewById(R.id.document_item_icon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            ProgressDialog progressDialog;

            progressDialog = new ProgressDialog(documentsActivity);
            progressDialog.setMessage("A message");
            progressDialog.setIndeterminate(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(true);

            final AsyncDownload downloadTask = new AsyncDownload(documentsActivity,progressDialog);
            downloadTask.execute(data.get(getAdapterPosition()).url,data.get(getAdapterPosition()).name);

            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    downloadTask.cancel(true);
                }
            });
        }
    }

}

