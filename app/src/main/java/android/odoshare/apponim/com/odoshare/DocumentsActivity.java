package android.odoshare.apponim.com.odoshare;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.odoshare.apponim.com.odoshare.controller.AppController;
import android.odoshare.apponim.com.odoshare.entities.DocumentEntity;
import android.odoshare.apponim.com.odoshare.file_requests.AsyncUpload;
import android.odoshare.apponim.com.odoshare.recycler_view.DocumentListAdapter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class DocumentsActivity extends AppCompatActivity {

    private static final int INTENT_FILE_CHOOSER_CODE = 551;

    // intent parameters
    private int locationId;
    private String locationName;
    private double latitude;
    private double longitude;

    // recycler view
    RecyclerView recyclerView;
    DocumentListAdapter recyclerViewAdapter;

    // views
    LinearLayout progressContainer;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents);

        // get intent parameters
        Intent myIntent = getIntent();
        locationId = myIntent.getIntExtra("locationId", 0);
        locationName = myIntent.getStringExtra("locationName");
        latitude = myIntent.getDoubleExtra("latitude", 0);
        longitude = myIntent.getDoubleExtra("longitude", 0);

        // initialize toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(locationName);
        setSupportActionBar(toolbar);

        // initialize recyclerview
        recyclerView = (RecyclerView) findViewById(R.id.documents_rec_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // initialize other views
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.document_coordinator_layout);
        progressContainer = (LinearLayout) findViewById(R.id.documents_progress_container);

        loadDocuments();

        // initialize fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
    }

    /**
     * Retrieves from server all of the documents for locations with {@link #locationId} id value.
     */
    public void loadDocuments() {

        String URL_LOCATIONS = "http://odoshare.azurewebsites.net/api/locations/";
        String URL_DOCUMENTS = "/documents";

        String url = URL_LOCATIONS + locationId + URL_DOCUMENTS;

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d("JSON", response.toString());

                try {

                    List<DocumentEntity> data = new ArrayList<>();

                    for (int i = 0; i < response.length(); i++) {
                        DocumentEntity item = new DocumentEntity();

                        item.id = response.getJSONObject(i).getInt("id");
                        item.name = response.getJSONObject(i).getString("name");
                        item.url = response.getJSONObject(i).getString("url");

                        data.add(item);
                    }

                    updateRecyclerView(data);

                    // hide progress bar
                    recyclerView.setVisibility(View.VISIBLE);
                    progressContainer.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("JSON", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

                Log.e("ERROR", error.getMessage());
            }
        });

        // show progress bar
        recyclerView.setVisibility(View.GONE);
        progressContainer.setVisibility(View.VISIBLE);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    /**
     * Updates {@link #recyclerView} with given data
     * @param data Data to be put into recycler view
     */
    private void updateRecyclerView(List<DocumentEntity> data) {
        recyclerViewAdapter = new DocumentListAdapter(this, data);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    /**
     * Opens intent with file choosing functionality.
     */
    private void showFileChooser() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"),
                    INTENT_FILE_CHOOSER_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case INTENT_FILE_CHOOSER_CODE:
                if (resultCode == RESULT_OK) {

                    Uri uri = data.getData();
                    Log.d("DOCUMENT ACTIVITY", "File Uri: " + uri.toString());

                    String path = null;

                    try {
                        path = getPath(this, uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    Log.d("DOCUMENT ACTIVITY", "File Path: " + path);

                    new AsyncUpload(path, locationId, this).execute();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Returns path to selected uri
     * @param context Context
     * @param uri URI
     * @return String representation of path
     * @throws URISyntaxException
     */
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Shows snackbar with open file action.
     * @param filePath Path to file to be opened
     */
    public void showOpenFilePopup(final String filePath) {

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "File downloaded", Snackbar.LENGTH_LONG)
                .setAction("OPEN", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        File file = new File(filePath);
                        MimeTypeMap map = MimeTypeMap.getSingleton();
                        String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
                        String type = map.getMimeTypeFromExtension(ext);

                        if (type == null)
                            type = "*/*";

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri data = Uri.fromFile(file);

                        intent.setDataAndType(data, type);

                        if (intent.resolveActivity(getPackageManager()) != null)
                            startActivity(intent);
                        else {
                            Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "No default application", Snackbar.LENGTH_LONG);
                            snackbar1.show();
                        }

                    }
                });

        snackbar.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_documents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_open_in_maps) {

            Uri gmmIntentUri = Uri.parse("geo:" + latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }

            return true;
        }

        if (id == R.id.action_open_direction) {

            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
