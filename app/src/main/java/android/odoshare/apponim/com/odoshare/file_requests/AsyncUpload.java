package android.odoshare.apponim.com.odoshare.file_requests;

import android.odoshare.apponim.com.odoshare.DocumentsActivity;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ivan on 9/25/2016.
 * <p>
 * This class is property of APPONIM
 * apponim.com
 */
public class AsyncUpload extends AsyncTask {

    private static final String SERVER_URL = "http://odoshare.azurewebsites.net/api/locations/";
    private static final String STORAGE_URL = "/documents/upload";

    private String path;
    private int locationId;
    private DocumentsActivity documentsActivity;

    public AsyncUpload(String path, int locationId, DocumentsActivity documentsActivity) {
        this.path = path;
        this.locationId = locationId;
        this.documentsActivity = documentsActivity;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String charset = "UTF-8";
        File uploadFile1 = new File(path);

        String requestURL = SERVER_URL + locationId + STORAGE_URL;

        Log.d("STORAGE URL", requestURL);

        try {
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);

            multipart.addFilePart("file", uploadFile1);

            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");

            for (String line : response) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        documentsActivity.loadDocuments();

        Log.d("ASYNC UPLOAD", "FINISHED");
    }
}
