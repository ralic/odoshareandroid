package android.odoshare.apponim.com.odoshare.entities;

/**
 * Created by Ivan on 9/24/2016.
 * <p/>
 * This class is property of APPONIM
 * apponim.com
 */
public class DocumentEntity {

    public int id;
    public String name;
    public String url;

}
