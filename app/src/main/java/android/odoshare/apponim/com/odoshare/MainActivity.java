package android.odoshare.apponim.com.odoshare;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.odoshare.apponim.com.odoshare.controller.AppController;
import android.odoshare.apponim.com.odoshare.entities.LocationEntity;
import android.odoshare.apponim.com.odoshare.gps.GPSHelper;
import android.odoshare.apponim.com.odoshare.recycler_view.LocationListAdapter;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final double COORDINATE_RANGE = 3.0;

    RecyclerView recyclerView;
    LocationListAdapter recyclerViewAdapter;

    LinearLayout progressContainer;

    GPSHelper gpsHelper;
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gpsHelper = new GPSHelper(this);

        recyclerView = (RecyclerView) findViewById(R.id.main_rec_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressContainer = (LinearLayout) findViewById(R.id.main_progress_container);

        loadLocations();

        // floating action button initialization
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGPSLocation();
                Toast.makeText(MainActivity.this, latitude + ":" + longitude, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Loads all locations from server into recyclerview
     */
    private void loadLocations() {
        loadLocations(null);
    }

    /**
     * Loads near locations from server into recyclerview. If location is null then loads
     * all of the locations from server into recyclerview
     * @param location Current GPS location of user
     */
    private void loadLocations(Location location) {

        String URL_LOCATIONS = "http://odoshare.azurewebsites.net/api/locations";

        String url;

        if (location != null) {

            double lat = location.getLatitude();
            double lon = location.getLongitude();

            url = URL_LOCATIONS +
                    "?latMin=" + (lat - COORDINATE_RANGE) +
                    "&latMax=" + (lat + COORDINATE_RANGE) +
                    "&lonMin=" + (lon - COORDINATE_RANGE) +
                    "&lonMax=" + (lon + COORDINATE_RANGE);
        } else
            url = URL_LOCATIONS;

        Log.d("URL", url);

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d("JSON", response.toString());

                try {

                    List<LocationEntity> data = new ArrayList<>();

                    for (int i = 0; i < response.length(); i++) {
                        LocationEntity item = new LocationEntity();

                        item.id = response.getJSONObject(i).getInt("id");
                        item.name = response.getJSONObject(i).getString("name");
                        item.latitude = response.getJSONObject(i).getDouble("latitude");
                        item.longitude = response.getJSONObject(i).getDouble("longitude");
                        item.fileCount = response.getJSONObject(i).getInt("filesCount");

                        data.add(item);
                    }

                    updateRecyclerView(data);

                    // hide progress bar
                    recyclerView.setVisibility(View.VISIBLE);
                    progressContainer.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("JSON", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        // show progress bar
        recyclerView.setVisibility(View.GONE);
        progressContainer.setVisibility(View.VISIBLE);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    /**
     * Sends POST request to server to add new user location.
     * @param name Name of current user location
     */
    public void addNewLocation(final String name) {

        String URL_LOCATIONS_ADD = "http://odoshare.azurewebsites.net/api/locations/add";

        String url = URL_LOCATIONS_ADD;

        Log.d("URL CHECK", url);

        JSONObject locationJson = new JSONObject();

        try {
            locationJson = new JSONObject();

            locationJson.put("name", name);
            locationJson.put("latitude", latitude);
            locationJson.put("longitude", longitude);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // initialize json request
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, locationJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSON", response.toString());

                        Location location = new Location("");
                        location.setLongitude(longitude);
                        location.setLatitude(latitude);

                        loadLocations(location);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Volley", error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    /**
     * Retrieves GPS location from GPS sensor and puts it to {@link #latitude} and
     * {@link #longitude} variables. If GPS sensor is disabled pops up dialog.
     * @return If retrieve of GPS is successful returns true else returns false
     */
    private boolean getGPSLocation() {

        if (gpsHelper.canGetLocation()) {

            latitude = gpsHelper.getLatitude();
            longitude = gpsHelper.getLongitude();

            if (latitude != 0 || longitude != 0) {
                Location location = new Location("");
                location.setLatitude(latitude);
                location.setLongitude(longitude);

                loadLocations(location);

                return true;
            }
        } else {
            showGpsDialog();
        }

        return false;
    }

    /**
     * Updates RecyclerView with given data.
     * @param data Data do be set into recyclerView
     * @see #recyclerView
     */
    private void updateRecyclerView(List<LocationEntity> data) {
        recyclerViewAdapter = new LocationListAdapter(this, data);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    /**
     * Pops up GPS not enabled dialog
     */
    private void showGpsDialog() {

        // initialize alert dialog builder
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        // initialize dialog
        dialog.setTitle("GPS disabled")
                .setMessage("Please enable your GPS to use this action.")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });

        // show dialog
        dialog.show();
    }

    /**
     * Pops up dialog for adding new location.
     */
    public void showNewLocationDialog() {

        // initialize alert dialog builder
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        // initialize edittext
        final EditText locationNameTextView = new EditText(this);
        locationNameTextView.setHint("Location name");

        // initialize dialog
        alert.setTitle("Create new location")
                .setMessage("Upload current GPS location as new FILE SHARING location\n")
                .setView(locationNameTextView)
                .setPositiveButton("UPLOAD THIS LOCATION", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        if (gpsHelper.canGetLocation()) {
                            latitude = gpsHelper.getLatitude();
                            longitude = gpsHelper.getLongitude();
                            addNewLocation(locationNameTextView.getText().toString());
                        } else {
                            showGpsDialog();
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        // show dialog
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_location) {
            showNewLocationDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
