package android.odoshare.apponim.com.odoshare.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivan on 9/24/2016.
 * <p/>
 * This class is property of APPONIM
 * apponim.com
 */
public class LocationEntity {

    public int id;
    public String name;
    public double latitude;
    public double longitude;
    public int fileCount;

    public List<DocumentEntity> documentEntities = new ArrayList<>();

    public LocationEntity(){}

    public LocationEntity(String name, double latitude, double longitude, int fileCount) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.fileCount = fileCount;
    }
}
